//
//  NSString+Emoji.h
//  DynamismDemo
//
//  Created by James Cash on 08-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Emoji)

- (NSString*)addEnthusiasm;

@end
