//
//  Monorail.m
//  DynamismDemo
//
//  Created by James Cash on 08-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Monorail.h"

@implementation Monorail

- (void)drive
{
    NSLog(@"All aboard!");
    [self.delegate boardVehicle:self];

    NSLog(@"driving along");

    NSLog(@"Oh no, a Telsa fell from space & landed on the tracks?");

    NSLog(@"do we have a monorail stunt driver aboard?");

    // we need to check if our delegate respondsToSelector, because it's allowed to not implement the method & if we call a method on an object that doesn't implement it, we get a crash
    if ([self.delegate respondsToSelector:@selector(performEmergencyManeouvers:)]) {
        // note that we *don't* need to use performSelector: here because the delegate object implements the protocol, even though the method is optional, XCode will always "let" us call the method on the object
        [self.delegate performEmergencyManeouvers:self];
    } else {
        NSLog(@"Oops, ran it over");
    }
}

@end
