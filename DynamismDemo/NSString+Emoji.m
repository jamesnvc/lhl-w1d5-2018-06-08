//
//  NSString+Emoji.m
//  DynamismDemo
//
//  Created by James Cash on 08-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "NSString+Emoji.h"

@implementation NSString (Emoji)

- (NSString *)addEnthusiasm
{
    return [self stringByAppendingString:@"👍🏻"];
}

@end
