//
//  Passenger.m
//  DynamismDemo
//
//  Created by James Cash on 08-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Passenger.h"

@implementation Passenger

- (void)boardVehicle:(Monorail *)monorail {
    NSLog(@"Running up the stairs of %@", monorail);
}

@end
