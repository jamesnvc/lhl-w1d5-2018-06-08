//
//  Monorail.h
//  DynamismDemo
//
//  Created by James Cash on 08-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

// we put this here to "forward-declare" the monorail class, because the MonorailRider protocol needs to know that this is a valid class name but the Monorail class needs to know that the MonorailRider is a valid protocol name
// so to break this "circular dependency" (i.e. two things both need to know that the other exists before it can be created) we do this @class, which says "a class called Monorail will exist later; I promise the details will be filled in by the time you need to know"
@class Monorail;

@protocol MonorailRider <NSObject>

@required

- (void)boardVehicle:(Monorail*)monorail;

@optional

- (void)performEmergencyManeouvers:(Monorail*)monorail;

@end

@interface Monorail : NSObject

@property (nonatomic, strong) id<MonorailRider> delegate;

- (void)drive;

@end
