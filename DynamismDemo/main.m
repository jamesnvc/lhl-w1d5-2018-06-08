//
//  main.m
//  DynamismDemo
//
//  Created by James Cash on 08-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Emoji.h"
#import "Player.h"
#import "Monorail.h"
#import "Passenger.h"
#import "Driver.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // making an NSMutableArray, but storing it in an NSArray variable
        // we can do this, because NSMutableArray is a *subclass* of NSArray
        // which means that every NSMutableArray is an NSArray
        NSArray *array = [@[@"foo", @"bar"] mutableCopy];
//        NSArray *array = @[@"foo", @"bar"];

        // try switching out the above lines to see that the "baz" string only gets added when it's a mutable array, but if it's immutable it doesn't crash, just doesn't try to add the string

        // xcode complains about this
//        [array addObject:@"baz"];
        if ([array respondsToSelector:@selector(addObject:)]) {
            [array performSelector:@selector(addObject:) withObject:@"baz"];
            // ^ this is equivalent to
            // [array addObject:@"baz"]
            // but XCode still won't let us write the above, even in this "if"
            // because XCode is doing a purely "Static" analysis of the code
            // so it can't really figure out that if control reaches the inside of this if, the code must be valid
        }

        NSLog(@"Array: %@", array);

        // string category
        NSLog(@"%@", [@"foo" addEnthusiasm]);


        // player example
        Player *p = [[Player alloc] init];
        [p fight];
        NSLog(@"health now %ld", p.health);

//        [p fightTimes];
        NSNumber* times = [p performSelector:@selector(fightTimes)];
        NSLog(@"Stole fight times %@", times);
        [p performSelector:@selector(setFightTimes:) withObject:@(3)];
        [p fight];

        // delegation & protocols

        Monorail *mono = [[Monorail alloc] init];
        [mono drive];
        Passenger *pass = [[Passenger alloc] init];
        mono.delegate = pass;
        [mono drive];
        Driver *driver = [[Driver alloc] init];
        mono.delegate = driver;
        [mono drive];
    }
    return 0;
}
