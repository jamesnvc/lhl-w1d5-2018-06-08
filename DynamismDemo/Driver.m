//
//  Driver.m
//  DynamismDemo
//
//  Created by James Cash on 08-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Driver.h"

@implementation Driver

- (void)boardVehicle:(Monorail *)monorail
{
    NSLog(@"Stroll into the cabin of %@", monorail);
}

- (void)performEmergencyManeouvers:(Monorail *)monorail
{
    NSLog(@"Throw an old-timey dynamite stick in front to make the whole train jump up. It is super rad");
}

@end
