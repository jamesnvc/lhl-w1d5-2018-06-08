//
//  Player.m
//  DynamismDemo
//
//  Created by James Cash on 08-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Player.h"

@interface Player ()

@property (nonatomic,strong) NSNumber *fightTimes;

@end

@implementation Player

- (instancetype)init
{
    self = [super init];
    if (self) {
        _health = 10;
    }
    return self;
}

- (void)fight {
    _health -= 1;
    NSLog(@"Why would you do this?");
    // note that the below line is really calling `[self setFightTimes:@(...)]`
    // some complier "magic" does the translation for us
    self.fightTimes = @(self.fightTimes.intValue + 1);
    NSLog(@"You've fought %@ times, you should be ashamed of yourself", self.fightTimes);
}

@end
